from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from client.models import Client


# Create your models here.
class Categorie(models.Model):
    category_name = models.CharField(max_length=200, db_index=True, default='Categorie', null=False,
                                     verbose_name='Categorie')
    slug = models.SlugField(max_length=200, db_index=True)

    def __str__(self):
        return self.category_name

    class Meta:
        ordering = ('category_name',)
        index_together = (('id', 'slug'),)
        verbose_name = "Categorie des établissements"
        verbose_name_plural = "Catégories des établissements"


class Etablissement(models.Model):
    category = models.ForeignKey(Categorie, on_delete=models.CASCADE, verbose_name='Categorie de votre etablissement')
    client = models.ForeignKey(Client, on_delete=models.CASCADE, db_index=True, verbose_name='client en charge')
    name = models.CharField(max_length=200, db_index=True, null=False, default='Etablissement',
                            help_text='Le nom de votre etablissement')
    slug = models.SlugField(max_length=200, db_index=True)
    country = models.CharField(max_length=200, db_index=True, null=False, default='Algérie', verbose_name='Pays')
    city = models.CharField(max_length=100, db_index=True, null=False, default='Alger', verbose_name='Ville')
    commune = models.CharField(max_length=100, db_index=True, null=False, default='Alger centre')
    address = models.CharField(max_length=100, db_index=True, null=False, default='adresse de votre etablissement',
                               verbose_name='adresse de votre etablissement')
    photo = models.ImageField(upload_to='users/%Y/%m/%d',
                              verbose_name="Ajoutez une photo ou le logo de votre etablissement (Facultatif)",
                              blank=True)
    num_acceuil = PhoneNumberField(blank=True, verbose_name="Numero de telephone")
    nom_acceuil = models.CharField(max_length=250, verbose_name="Nom de la personne responsable de l'acceuil", default='acceuil')
    details = models.TextField(blank=True, null=True, help_text='Donnez plus de details sur votre etablissement')
    num_registre = models.CharField(max_length=30, null=False, default="Registre de commerce",
                                    verbose_name="Numero du registre de commerce")
    NIF = models.CharField(max_length=30, null=False, default="Numero NIF",
                           help_text="Entrez le NIF de votre etablissement")
    NIS = models.CharField(max_length=30, null=False, default="Numero NIS",
                           help_text="Entrez le NIS de votre etablissement")
    article = models.CharField(max_length=30, verbose_name="Article d'imposition", help_text="Entrez votre numéro d'article d'imposition", default='numéro article')
    disponible = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)
        verbose_name = "Etablissement"
        verbose_name_plural = "Etablissements"
