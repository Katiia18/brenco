from django.contrib import admin
from.models import *

# Register your models here.

class CategorieAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('category_name',), }
    list_filter = ('category_name', )
    search_fields = ('category_name', )
    readonly_fields=('slug', )

admin.site.register(Categorie, CategorieAdmin)

class EtablissementAdmin(admin.ModelAdmin):
    #repartition sur la page
    fieldsets = [
        ('Client', {'fields': ['client']}),
        ('Informations générales',{'fields': ['category','name', 'slug', 'num_registre', 'NIF', 'NIS', 'article']}),
        ('Adresse', {'fields': ['country', 'city', 'commune', 'address']}),
        ('Informations suplémentaires', {'fields': ['photo','num_acceuil', 'details']})
    ]
    list_display = ('name', 'client', 'category', 'country', 'city', 'commune')
    prepopulated_fields = {'slug': ('name',), }
    list_filter = ['client', 'category', 'country', 'city', 'commune']
    search_fields = ['name', 'country', 'city', 'commune', 'num_registre', 'article']

admin.site.register(Etablissement, EtablissementAdmin)
