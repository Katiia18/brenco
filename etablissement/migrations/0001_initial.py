# -*- coding: utf-8 -*-
# Generated by Django 1.11.26 on 2020-01-30 15:12
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('client', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categorie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category_name', models.CharField(db_index=True, default='Categorie', max_length=200, verbose_name='Categorie')),
                ('slug', models.SlugField(max_length=200)),
            ],
            options={
                'verbose_name': 'Categorie des établissements',
                'verbose_name_plural': 'Catégories des établissements',
                'ordering': ('category_name',),
            },
        ),
        migrations.CreateModel(
            name='Etablissement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, default='Etablissement', help_text='Le nom de votre etablissement', max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('country', models.CharField(db_index=True, default='Algérie', max_length=200, verbose_name='Pays')),
                ('city', models.CharField(db_index=True, default='Alger', max_length=100, verbose_name='Ville')),
                ('commune', models.CharField(db_index=True, default='Alger centre', max_length=100)),
                ('address', models.CharField(db_index=True, default='adresse de votre etablissement', max_length=100, verbose_name='adresse de votre etablissement')),
                ('photo', models.ImageField(blank=True, upload_to='users/%Y/%m/%d', verbose_name='Ajoutez une photo ou le logo de votre etablissement (Facultatif)')),
                ('num_acceuil', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, verbose_name='Numero de telephone')),
                ('details', models.TextField(blank=True, help_text='Donnez plus de details sur votre etablissement', null=True)),
                ('num_registre', models.CharField(default='Registre de commerce', max_length=30, verbose_name='Numero du registre de commerce')),
                ('NIF', models.CharField(default='Numero NIF', help_text='Entrez le NIF de votre etablissement', max_length=30)),
                ('NIS', models.CharField(default='Numero NIS', help_text='Entrez le NIS de votre etablissement', max_length=30)),
                ('disponible', models.BooleanField(default=True)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='etablissement.Categorie', verbose_name='Categorie de votre etablissement')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='client.Client', verbose_name='client en charge')),
            ],
            options={
                'verbose_name': 'Etablissement',
                'verbose_name_plural': 'Etablissements',
                'ordering': ('name',),
            },
        ),
        migrations.AlterIndexTogether(
            name='categorie',
            index_together=set([('id', 'slug')]),
        ),
        migrations.AlterIndexTogether(
            name='etablissement',
            index_together=set([('id', 'slug')]),
        ),
    ]
