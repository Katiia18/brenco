# accounts.models.py

from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.utils import timezone

class UserManager(BaseUserManager):
    def create_user(self, email, nom, prenom , password=None, is_active=True, is_staff=False, is_admin=False):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("L'utilisateur doit avoir une adresse email !")
        if not password:
            raise ValueError("L'utilisateur doit avoir un mot de passe")
        if not nom:
            raise ValueError("L'utilisateur doit avoir un nom")
        if not prenom:
            raise ValueError("L'utilisateur doit avoir un prénom")
        user = self.model(
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.active = is_active
        user.staff = is_staff
        user.admin = is_admin
        user.date_joined = timezone.now()
        user.save(using=self._db)
        return user

    def create_staffuser(self, email,nom, prenom, password, date_joined=timezone.now()):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email=email,
            nom=nom,
            prenom=prenom,
            password=password,
        )
        user.date_joined= date_joined
        user.active = True
        user.staff = True
        user.admin = False
        user.save(using=self._db)
        return user

    def create_superuser(self, email,nom, prenom, password, date_joined=timezone.now()):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email=email,
            nom=nom,
            prenom=prenom,
            password=password,
        )
        user.date_joined = date_joined
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='E-mail',
        max_length=255,
        unique=True,
    )
    nom = models.CharField(max_length=255, verbose_name="Nom", null=True)
    prenom = models.CharField(max_length=255, verbose_name="Prénom", null=True)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now, verbose_name="Date d'inscritpion")

    # notice the absence of a "Password field", that's built in.

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['nom', 'prenom',] # Email & Password are required by default.

    def get_full_name(self):
        # The user is identified by their email address
        return (self.nom+self.prenom)

    def get_short_name(self):
        # The user is identified by their email address
        return self.prenom

    def __str__(self):              # __unicode__ on Python 2
        return (self.nom+" "+self.prenom)

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    @property
    def is_active(self):
        "Is the user active?"
        return self.active


    objects = UserManager()
