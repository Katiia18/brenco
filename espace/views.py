from django.shortcuts import render, get_object_or_404
from espace.models import *

# Create your views here.


def space_list(request, usage_slug=None):
    etablissement = None
    usages = Usage.objects.all()
    espaces = Space.objects.filter(available=True)
    if usage_slug:
        usages = get_object_or_404(Usage, slug=usage_slug)
    espaces = Space.filter(usage=usages)
    return render(request,
                  'space_shop/space/list.html',
                  {'etablissement': etablissement,
                   'usages': usages,
                   'espaces': espaces})


