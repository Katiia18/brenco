from django.contrib import admin
from .models import *

# Register your models here.

admin.site.site_header = 'THE OFFICE'
admin.site.site_title = 'Page Admin'
admin.site.index_title = 'BIENVENUE SUR LA PAGE ADMIN DE THE OFFICE !'

class DispositionAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
    readonly_fields = ('slug', )

admin.site.register(Disposition, DispositionAdmin)

class UsageAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Usage, UsageAdmin)

class SpaceImageInline(admin.TabularInline):
    model = SpaceImage
    extra = 3

class SpaceHoraireInline(admin.TabularInline):
    model = SpaceHoraire
    extra = 1

class SpaceAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Informations générales', {'fields': ['etablissement','name','slug','usage', 'photo_couverture']}),
        ('Disposition et capacité', {'fields': ['disposition', 'capacite', 'dimention']}),
        ('Tarification', {'fields': ['prix1', 'prix2', 'prix3']}),
        ('informations suplémentaires', {'fields': ['annonce', 'num_acceuil','nom_acceuil', 'caracteristiques',
                                                    'equipements','description', 'available', 'created', 'updated']}),
    ]
    inlines = [SpaceHoraireInline,SpaceImageInline, ]
    readonly_fields = ("created","updated",)
    list_display = ['name', 'usage', 'disposition', 'capacite', 'dimention','prix1','available', 'created', 'updated']
    list_filter = ['capacite', 'name', 'dimention', 'usage', 'created', 'disposition', 'available','prix1', 'prix2', 'prix3', 'updated']
    list_editable = ['available']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Space, SpaceAdmin)
