from django.core.exceptions import ValidationError
from django.db import models
from multiselectfield import MultiSelectField
from etablissement.models import Etablissement
from phonenumber_field.modelfields import PhoneNumberField
from djmoney.models.fields import MoneyField
from django.db import models
import datetime
from django.core.urlresolvers import reverse
from theOffice import settings


# Create your models here.
class Disposition(models.Model):
    name = models.CharField(max_length=250, verbose_name="Disposition de la salle", help_text="Exemple : Salle en U")
    slug = models.SlugField(max_length=250)

    def __str__(self):
        return self.name




class Usage(models.Model):
    name = models.CharField(max_length=250, verbose_name="Type d'usage", help_text="Exemple : Salle de réunion")
    slug = models.SlugField(max_length=250)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('space_shop:space_list_by_usage',
                       args=[self.slug])


CARACT_CHOICES = (
    ("Lumiére du jour", ("Lumiére du jour")),
    ("Climatisation", ("Climatisation")),
    ("Acces Handicapés", (" Acces Handicapés")),
    ("Espace de pause", ("Espace de pause")),
    ("Acces terrasse/Balcon", ("Acces terrasse/Balcon")),
    ("Cuisine partagée", ("Cuisine partagée")),
    ("Personne à l'acceuil", ("Personne à l'acceuil")),
)

EQUIPEMENT_CHOICES = (
    ("WIFI", ("WIFI")),
    ("Video projecteur", ("Video projecteur")),
    ("Micro + sonorisation", ("Micro + sonorisation")),
    ("Ecran de projection", ("ecran de projection")),
    ("Tableau blanc", ("Tableau blanc")),
    ("Ordinateur", ("Ordinateur")),
    ("Televiseur", ("Televiseur")),
    ("Prises electriques", ("Prises electriques")),
    ("Photocopie", ("Photocopie")),
    ("café inclus", ("café inclus")),
    ("stylos et bloc notes ", ("stylos et blocs note")),
    ("Bouteilles d'eau", ("Bouteilles d'eau"))
)


class Space(models.Model):
    etablissement = models.ForeignKey(Etablissement, on_delete=models.CASCADE,
                                      verbose_name='Etablissement en charge', null=True)
    name = models.CharField(max_length=200, null=True, db_index=True, help_text="Entrez le nom de votre salle ")
    slug = models.SlugField(max_length=200, db_index=True)
    usage = models.ForeignKey(Usage, on_delete=models.CASCADE, null=True)
    disposition = models.ForeignKey(Disposition, on_delete=models.CASCADE, null=True)
    photo_couverture = models.ImageField(upload_to='media/space_cover')
    prix1 = models.PositiveIntegerField(help_text='Entrez le prix de la location par demi heure', verbose_name="prix par demi heure")
    prix2 = models.PositiveIntegerField(help_text="Entrez le prix de la location pour une demi journée", verbose_name="prix par demi journée")
    prix3 = models.PositiveIntegerField(help_text="Entrez le prix de la location pour une journée complete", verbose_name="prix de la journée")
    annonce = models.CharField(max_length=250, null=False, default="Annonce espace",
                               help_text="Le titre de votre annonce")
    num_acceuil = PhoneNumberField(blank=True, verbose_name="Numéro de l'acceuil",
                                   help_text='Numéro de la personne qui recevera les clients')
    nom_acceuil = models.CharField(max_length=250, verbose_name='Nom de la personne ', help_text="Nom de la personne responsable de l'acceuil des clients")
    dimention = models.PositiveIntegerField(null=False, default='0',
                                            help_text="Entrez la superficie de votre salle en m2")
    capacite = models.PositiveIntegerField(null=False, default='1', help_text="nombre de personnes suporté par l'éspace")
    caracteristiques = MultiSelectField(choices=CARACT_CHOICES)
    equipements = MultiSelectField(choices=EQUIPEMENT_CHOICES)
    description = models.TextField(default="Location de salle", help_text="Donnez plus de détails sur votre espace")
    available = models.BooleanField(default=True, verbose_name='Disponible',
                                    help_text="décochez si cet espace n'est pas disponible")
    created = models.DateTimeField(auto_now=True, verbose_name='crée')
    updated = models.DateTimeField(auto_now=True, verbose_name='mis à jour')

    def __str__(self):
        return self.name
    """
    reverse for detail template 
    def get_absolute_url(self):
        return reverse(':space_detail',
                       args=[self.id, self.slug])
    """

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)
        verbose_name = "Espace"
        verbose_name_plural = "Espaces"


class SpaceImage(models.Model):
    space = models.ForeignKey(Space, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to='media/espaces_pcitures', help_text='inserer une photo de votre espace')
    name = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Images de l'espace"
        verbose_name_plural = "Images des espaces"

def no_past(value):
    today = datetime.date.today()
    if value < today:
        raise ValidationError('Choisissez une date supérieure à celle-ci !')

class SpaceHoraire(models.Model):
    space = models.ForeignKey(Space, on_delete=models.CASCADE, related_name='horaires')
    ouvert = models.BooleanField(default=True)
    date = models.DateField(default=datetime.date.today, validators=[no_past])
    ouverture = models.TimeField(null=False, default='08:00')
    fermeture = models.TimeField(null=False, default='19:00')

    def __str__(self):
        return str(self.date)

    class Meta:
        verbose_name = "Ajouter disponibilité de l'espace"
        verbose_name_plural = "Disponibilités de l'espace"

