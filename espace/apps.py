from django.apps import AppConfig


class EspaceConfig(AppConfig):
    name = 'espace'
