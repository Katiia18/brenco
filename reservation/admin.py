from django.contrib import admin
from.models import *

# Register your models here.

class ReservationAdmin(admin.ModelAdmin):
    list_display = ['nom','client', 'espace', 'horraire', 'validée','prix', 'etablissement',]
    list_filter = ('client','espace','horraire', 'etablissement', 'validée', 'prix' )
    search_fields = ('client', 'espace','etablissement', 'nom', 'horraire' )
    list_editable = ['validée']


admin.site.register(Reservation, ReservationAdmin)
