from django.db import models
from client.models import Client
from espace.models import Space, SpaceHoraire
from etablissement.models import Etablissement
# Create your models here.
class Reservation(models.Model):
    nom = models.CharField(max_length=250, help_text="entrez le nom de la reservation")
    client = models.OneToOneField(Client , on_delete=models.CASCADE, verbose_name="Client", help_text="Client qui va reserver l'espace")
    espace = models.OneToOneField(Space, on_delete=models.CASCADE, help_text='espace à reserver')
    etablissement = models.OneToOneField(Etablissement, on_delete=models.CASCADE, help_text="etablissement en charge de l'espace")
    horraire = models.ForeignKey(SpaceHoraire, on_delete= models.CASCADE, help_text="selectionnez les dates de reservation")
    remarque = models.TextField(default="remarques", help_text="Si vous avez des remarques ou des questions à faire")
    validée = models.BooleanField(default=False)
    prix = models.PositiveIntegerField(null=False, default='0', help_text='Prix total de la reservation')

    def __str__(self):
        return self.nom