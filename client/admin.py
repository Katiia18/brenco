from django.contrib import admin
from .models import *


# Register your models here.
class SecteurAdmin(admin.ModelAdmin):
    list_display = ['name', ]
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Secteur, SecteurAdmin)


class FormeJuridiqueAdmin(admin.ModelAdmin):
    list_display = ['name', ]
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(FormeJuridique, FormeJuridiqueAdmin)


class ClientAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Utilisateur', {'fields': ['user']}),
        ('Photo de profil', {'fields': ['photo',]}),
        ('Informations personnelles', {'fields': ['sexe', 'secteur', 'forme', 'raison_sociale', 'tel']})
    ]
    list_display = ('user','secteur', 'raison_sociale', 'tel')
    list_filter = ['user',  'raison_sociale', 'forme', 'raison_sociale']
    readonly_fields = ['image_tag',]
    search_fields = ['user', 'raison_sociale']


admin.site.register(Client, ClientAdmin)
