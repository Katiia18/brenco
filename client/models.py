from django.db import models
from django.utils.safestring import mark_safe

import accounts
from theOffice import settings
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class Secteur(models.Model):
    name = models.CharField(max_length=250, verbose_name="Secteur d'activité")
    slug = models.SlugField(max_length=250)

    def __str__(self):
        return self.name
    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)
        verbose_name = "Secteur d'acitivté"
        verbose_name_plural = "Secteurs d'activité"


class FormeJuridique(models.Model):
    name = models.CharField(max_length=250, verbose_name="Forme Juridique")
    slug = models.SlugField(max_length=250)

    def __str__(self):
        return self.name
    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)
        verbose_name = "Fomr Juridique"
        verbose_name_plural = "Formes Juridiques"

class Client(models.Model):
    CHOICES = (
        ('Madame', 'Mme'),
        ('Monsieur', 'Mr'),
    )
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Client') #heritates from user OneToOne
    sexe = models.CharField(max_length=20, verbose_name='Vous êtes', choices=CHOICES)
    raison_sociale = models.CharField(max_length=150, verbose_name='Votre raison sociale', null=False, default='Raison sociale')
    secteur = models.ForeignKey(Secteur, on_delete=models.CASCADE, null=True, verbose_name="secteur d'activité")
    forme = models.ForeignKey(FormeJuridique, on_delete=models.CASCADE, null=True, verbose_name="Forme juridique")
    tel = PhoneNumberField(null=True, verbose_name='Numéro de téléphone',help_text="Le numéro doit commencer par l'indicatif du pays. Ex:+213...")
    photo = models.ImageField(upload_to='media/users_pictures',verbose_name="Ajoutez une photo de profil (Facultatif)",
                              blank=True)

    def image_tag(self):
        return mark_safe('<img src="/users_pictures/%s" width="150" height="150" />' % (self.photo))

    image_tag.short_description = 'photo'

    def __str__(self):
        return 'Profil du client {}'.format(self.user.prenom)
